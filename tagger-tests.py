#!/usr/bin/env python3

import unittest
from tornado.testing import AsyncHTTPTestCase
import json

import tagger


class SimpleTest(AsyncHTTPTestCase):
    def get_app(self):
        return tagger.make_app()

    def test_livecheck(self):
        response = self.fetch('/')
        self.assertEqual(response.code, 200)

    def test_empty_event(self):
        response = self.fetch('/', method='POST', body='')
        self.assertEqual(response.code, 400)

    def test_empty_json(self):
        response = self.fetch('/', method='POST', body=json.dumps({}))
        self.assertEqual(response.code, 400)

    def test_dummy_merge_request(self):
        response = self.fetch('/', method='POST', body=json.dumps({'object_kind': 'merge_request', 'object_attributes': {'iid': 1}}))
        self.assertEqual(response.code, 200)

    def test_dummy_issue(self):
        response = self.fetch('/', method='POST', body=json.dumps({'object_kind': 'issue', 'object_attributes': {'iid': 1}}))
        self.assertEqual(response.code, 200)



if __name__ == '__main__':
    unittest.main()