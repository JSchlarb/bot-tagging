FROM python:3

COPY requirements.txt .
COPY tagger.py .
WORKDIR .
RUN pip install -r requirements.txt

EXPOSE 8080

CMD ["./tagger.py"]
