#!/usr/bin/env python3

import gitlab
import os
from datetime import datetime
import dateutil.parser
import coloredlogs, logging
import tornado.ioloop
import tornado.web
import json


logger = logging.getLogger(__name__)
coloredlogs.install(level=logging.INFO) #, logger=logger)


def _list(obj, **kwargs):
    result = [] 
    page = 1
    while len(obj.list(page=page, per_page=100, **kwargs)):
        result += obj.list(page=page, per_page=100, **kwargs)
        page += 1
    return result


def set_label_if(item, label, condition):
    if condition: # add label
        if label not in item.labels:
            logger.info(f"addind label '{label}'")
            item.labels.append(label)
    else: # remove label
        if label in item.labels:
            logger.info(f"removing label '{label}'")
            item.labels = [l for l in item.labels if l != label]
    return condition


def process_merge_request(merge_request):
    logger.info(f'MR {merge_request.iid}: "{merge_request.title}"')
    set_label_if(merge_request, 'ready_for_approval', merge_request.work_in_progress == False and merge_request.has_conflicts == False and merge_request.blocking_discussions_resolved == True)
    set_label_if(merge_request, 'merge_conflict', merge_request.has_conflicts == True)
    set_label_if(merge_request, 'unresolved_threads', merge_request.blocking_discussions_resolved == False)
    set_label_if(merge_request, 'draft', merge_request.work_in_progress == True)

    latest_note = merge_request.notes.list(sort='desc', order_by='updated_at', per_page=1, page=1)
    if len(latest_note) == 0: # no node activity
        updated_at = dateutil.parser.parse(merge_request.updated_at)
        set_label_if(merge_request, 'inactive', (datetime.today().date() - updated_at.date()).days > 30)
    else: # check latest merge request activity date
        updated_at = dateutil.parser.parse(latest_note[0].updated_at)
        set_label_if(merge_request, 'inactive', (datetime.today().date() - updated_at.date()).days > 30)

    merge_request.save()


def process_issue(issue):
    logger.info(f'Issue {issue.iid}: {issue.title}')
    merge_requests = issue.related_merge_requests()
    set_label_if(issue, 'ready_for_approval', len(merge_requests) != 0 and len([True for mr in merge_requests if mr['state'] in ['active', 'opened']]) == 0)

    latest_note = issue.notes.list(sort='desc', order_by='updated_at', per_page=1, page=1)
    if len(latest_note) == 0: # no node activity
        updated_at = dateutil.parser.parse(issue.updated_at)
        set_label_if(issue, 'inactive', (datetime.today().date() - updated_at.date()).days > 30)
    else: # check latest issue activity date
        updated_at = dateutil.parser.parse(latest_note[0].updated_at)
        set_label_if(issue, 'inactive', (datetime.today().date() - updated_at.date()).days > 30)

    if 'inactive' in issue.labels and 'ready_for_approval' in issue.labels:
        logger.info('closing inactive issue with all MR closed')
        issue.notes.create({'body': 'All associated merge-request are closed and there has been no recent activity. Closing the issue.'})
        issue.state_event = 'close'

    issue.save()


def periodic_check():
    for merge_request in _list(project.mergerequests, state='opened'):
        process_merge_request(merge_request)
    for issue in _list(project.issues, state='opened'):
        process_issue(issue)


class MainHandler(tornado.web.RequestHandler):
    def post(self):
        try:
            evt = tornado.escape.json_decode(self.request.body)
        except json.decoder.JSONDecodeError as ex:
            raise tornado.web.HTTPError(400, 'expecting JSON string')
        if 'object_kind' not in evt:
            raise tornado.web.HTTPError(400, "expecting Gitlab event with 'object_kind' field")
        logger.info(f"received event type '{evt['object_kind']}")
        if evt['object_kind'] == 'merge_request':
            merge_request = project.mergerequests.get(evt['object_attributes']['iid'])
            process_merge_request(merge_request)
        elif evt['object_kind'] == 'issue':
            issue = project.issues.get(evt['object_attributes']['iid'])
            process_issue(issue)
        elif evt['object_kind'] == 'note':
            if 'merge_request' in evt:
                merge_request = project.mergerequests.get(evt['merge_request']['iid'])
                process_merge_request(merge_request)
            elif 'issue' in evt:
                issue = project.issues.get(evt['issue']['iid'])
                process_issue(issue)
        else:
            logger.info(evt)

    def get(self):
        return


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

GITLAB_HOST = os.environ['GITLAB_HOST']
GITLAB_TOKEN = os.environ['GITLAB_TOKEN']
PORT = 8080
PROJECT_ID = 16504188

gl = gitlab.Gitlab(GITLAB_HOST, private_token=GITLAB_TOKEN)
project = gl.projects.get(PROJECT_ID)
logger.info(f'listening for event for {project.path_with_namespace}')
logger.info(f'listening on port {PORT}')


if __name__ == "__main__":
    if project.only_allow_merge_if_all_discussions_are_resolved != True:
        logger.error("MISSING CONFIGURATION https://docs.gitlab.com/ee/user/discussions/#only-allow-merge-requests-to-be-merged-if-all-threads-are-resolved")
        logger.warning("Setting 'only_allow_merge_if_all_discussions_are_resolved' to True for you")
        project.only_allow_merge_if_all_discussions_are_resolved = True
        project.save()

    app = make_app()
    app.listen(PORT)
    tornado.ioloop.PeriodicCallback(periodic_check, callback_time=1000*60*60).start()
    tornado.ioloop.IOLoop.current().start()
